package kz.aitu.chat.controller;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.service.AuthService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@AllArgsConstructor
@RequestMapping("/api/auth")
public class AuthController {
    private AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestHeader("login") String login, @RequestHeader("password") String password) {
        String token = authService.login(login, password);
        return ResponseEntity.ok(token);
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestHeader("name") String name, @RequestHeader("login") String login, @RequestHeader("password") String password) {
        Users users = authService.register(name, login, password);
        return ResponseEntity.ok(users);
    }

    @PostMapping("/authCheckLogin")
    public ResponseEntity<?> checkLogin(@RequestHeader("token") String token) {
        Users users = authService.checkLogin(token);
        return ResponseEntity.ok(users);
    }


    @GetMapping("")
    public ResponseEntity<?> getAll() {
        return ResponseEntity.ok(authService.findAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable Long id) {
        return ResponseEntity.ok(authService.findById(id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        authService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("")
    public ResponseEntity<?> addParticipant(@RequestBody Auth auth) {
        return ResponseEntity.ok(authService.save(auth));
    }
}
