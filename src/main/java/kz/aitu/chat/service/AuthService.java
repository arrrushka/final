package kz.aitu.chat.service;

import kz.aitu.chat.model.Auth;
import kz.aitu.chat.model.Chat;
import kz.aitu.chat.model.Users;
import kz.aitu.chat.repository.AuthRepository;
import kz.aitu.chat.repository.UsersRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@AllArgsConstructor
public class AuthService {
    private AuthRepository authRepository;
    private final UsersRepository usersRepository;

    public String login(String login, String password) {
        Auth auth = authRepository.findByLoginAndPassword(login, password);
        if (auth != null) {
            UUID uuid = UUID.randomUUID();
            String uuidAsString = uuid.toString();
            auth.setToken(uuidAsString);
            authRepository.save(auth);
            return auth.getToken();
        } else {
            return null;
        }
    }

    public Users register(String name, String login, String password) {
        Users user = usersRepository.findByName(name);
        if (user != null) {
            Auth auth = new Auth();
            auth.setUserId(user.getId());
            auth.setPassword(password);
            auth.setLogin(login);
            authRepository.save(auth);
            return user;
        } else {
            return null;
        }
    }

    public Users checkLogin(String token) {
        Auth auth = authRepository.findByToken(token);
        if (auth != null) {
            return usersRepository.findUserById(auth.getUserId());
        } else {
            return null;
        }
    }

    public boolean isTokenExist(String t) {
        if (authRepository.isExistsByToken(t)) {
            return true;
        }
        return false;
    }

    public void add(Auth auth) {
        authRepository.save(auth);
    }

    public List<Auth> findAll() {
        return authRepository.findAll();
    }

    public Optional<Auth> findById(Long id) {
        return authRepository.findById(id);
    }

    public Auth save(Auth auth) {
        return authRepository.save(auth);
    }

    public void deleteById(Long id) {
        authRepository.deleteById(id);
    }
}
